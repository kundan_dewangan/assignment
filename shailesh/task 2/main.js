var noOfRows = 0;
function generateRows() {
    noOfRows = document.getElementById('noOfRows').value;
    if(noOfRows > 0) {
        display(noOfRows);
    } else {
        alert('Please input more then 0 row.')
    }
}

function display(noOfRows) {
    var mainDiv = document.getElementById('main');
    mainDiv.innerHTML='';
    for(let i = 0; i < noOfRows; i++) {
        var rowDiv = '<div class="row">';
        for(let j=0; j<=i; j++) {
            var colDiv = '<div class="in-row-div"><div class="box '+(i+1)+'">'+(i+1)+'</div></div>';
            rowDiv+=colDiv;
        }
        rowDiv +='</div>';
        mainDiv.innerHTML += rowDiv;
    }
}

function changeColor() {
    var rowNo = parseInt(document.getElementById('txtRow').value);
    var txtColor = document.getElementById('txtColor').value;

    var r = parseInt(txtColor.substr(1,3),16);
    var g = parseInt(txtColor.substr(3,5),16);
    var b = parseInt(txtColor.substr(5,7),16);
    var yiq = ((r*255)+(g*255)+(b*255))/1000;
    var tc  = (yiq >= 5000) ? 'black' : 'white';
    console.log(yiq +" "+ tc);

    if(txtColor !== '' && rowNo > 0 && rowNo <= noOfRows) {
        var n = document.getElementsByClassName(rowNo);
        for(var a=0; a < n.length; a++) {
            document.getElementsByClassName(rowNo).item(a).style.backgroundColor=txtColor;
            document.getElementsByClassName(rowNo).item(a).style.color=tc;
        }
    } else {
        alert('Please input correct value!');
    }
}

function randomColor() {
    var rowNo = parseInt(document.getElementById('txtRow').value);
    if(rowNo > 0 && rowNo <= noOfRows) {
        var n = document.getElementsByClassName(rowNo);
        for(var a=0; a < n.length; a++) {
            // var r = Math.floor((Math.random() * 255) + 1);
            // var g = Math.floor((Math.random() * 255) + 1);
            // var b = Math.floor((Math.random() * 255) + 1);
            // var rgb = 'rgb('+r+','+g+','+b+')';
            var rgb = '#' + (function co(lor){
               return (lor += [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'][Math.floor(Math.random()*16)]) 
               && (lor.length == 6) ?  lor : co(lor); })('');
            var r = parseInt(rgb.substr(1,3),16);
            var g = parseInt(rgb.substr(3,5),16);
            var b = parseInt(rgb.substr(5,7),16);
            var yiq = ((r*255)+(g*255)+(b*255))/1000;
            var tc  = (yiq >= 5000) ? 'black' : 'white';

            document.getElementsByClassName(rowNo).item(a).style.backgroundColor= rgb;
            document.getElementsByClassName(rowNo).item(a).style.color=tc;
        }
    } else {
        alert('Please input correct value!');
    }
}