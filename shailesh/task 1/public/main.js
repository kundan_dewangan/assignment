var posMap = {};

function onloadPage() {
    localStorage.setItem('posMap', posMap);
    if(posMap === undefined) {
        posMap = {};
        console.log('Not find');
    } else {
        display(posMap);    
    }
}

function bodyClick(event) {
    console.log(event);
    if (event.srcElement.id === 'removeBtn' || event.srcElement.id === 'inputText' || 
        event.srcElement.id === 'saveText' || event.srcElement.className === 'span') {
        console.log('click');
    } else {
        var pgX = event.pageX ;
        var pgY = event.pageY ;
        posMap[event.pageX + ':' + event.pageY] = { 'cordX': pgX, 'cordY': pgY, 'text': 'Hello Shailesh!' };
        localStorage.setItem('posMap', posMap);
        console.log(posMap);
        var newElement = {
            'cordX': pgX,
            'cordY': pgY, 
            'text': 'Hello Shailesh!'
        };
        addElement(newElement);
    }
}

function display(posMap) {
    var bodyElm = '';
    console.log(JSON.stringify(posMap));
    for(var item in posMap) {
        var newElement = {
            'cordX': posMap[item].cordX,
            'cordY': posMap[item].cordY, 
            'text': posMap[item].text
        };
        addElement(newElement);
    }
    document.body.innerHTML = bodyElm
}

function addElement(newElement) {
    var position = "'"+newElement.cordX+":"+newElement.cordY+"'";
    var spanId = newElement.cordX+":"+newElement.cordY;
    var newSpan = '<span id='+spanId+' class="span" style="position:absolute; left:'+newElement.cordX+"px"+'; top:'+newElement.cordY+"px"+';" \
    onclick="editText('+position+')">'+newElement.text+'<img src="Delete.png" height="20px" width="20px" id="removeBtn" onclick="removeName('+position+')"></img></span>';
    document.body.innerHTML += newSpan;
}

function editText(item) {
    if(window.event.srcElement.id==='removeBtn') {

    } else {
        var position = "'"+posMap[item].cordX+":"+posMap[item].cordY+"'";
        var inputBox = '<span id="inputSpan" style="position:absolute; left:'+posMap[item].cordX+"px"+'; top:'+posMap[item].cordY+"px"+';" \
            ><input type="text" id="inputText" height="20px" value="'+posMap[item].text+
            '"><img src="Right.png" height="20px" width="20px" id="saveText" onclick="saveText('+position+')" value="Save"></button></span>';
        document.body.innerHTML += inputBox;
    }
}

function saveText(item) {
    var position = "'"+posMap[item].cordX+":"+posMap[item].cordY+"'";
    var id = posMap[item].cordX+":"+posMap[item].cordY;
    var text = document.getElementById('inputText').value;
    posMap[item].text = text;
    document.getElementById(item).innerHTML = text+'<img src="Delete.png" height="20px" width="20px" id="removeBtn" onclick="removeName('+position+')"></img>';
    var element = document.getElementById("inputSpan");
    element.outerHTML = "";
    delete element;
}

function removeName(item) {
    var position = "'"+posMap[item].cordX+":"+posMap[item].cordY+"'";
    console.log('remove '+position);
    delete posMap[item];
    console.log(posMap);
    var element = document.getElementById(item);
    element.outerHTML = "";
    delete element;
}