const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/mousedb';

MongoClient.connect(url, (err, db) => {
	if (err) {
		return console.log('Unable to connect to database');
	} 
	console.log('Connected to database');

	// db.collection('Positions').deleteMany({cordX:"400"}).then((result) => {
	// 	console.log('Positions');
	// 	console.log(JSON.stringify(result, undefined, 2));
	// }, (err) => {
	// 	console.log('Unable to fetch positions',err);
	// });

	db.collection('Positions').findOneAndDelete({cordX:"400"}).then((result) => {
		console.log('Positions');
		console.log(JSON.stringify(result, undefined, 2));
	}, (err) => {
		console.log('Unable to fetch positions',err);
	});

	db.close();
});