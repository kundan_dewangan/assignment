const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/mousedb';

MongoClient.connect(url, (err, db) => {
    if (err) {
        return console.log('Unable to connect to database');
    }
    console.log('Connected to database');
    db.collection('positions').insertOne({
    	'_id': '40:30',
    	'cordX': '40',
    	'cordY': '30',
    	'text': 'Hello shailesh'
    }, (err, result) => {
    	if(err) {
    		return console.log('Unable to insert in database', err);
    	}
    	console.log(result.ops);
    });

    db.close();
});