const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

var { mongoose } = require('./config/mongoose');
var { Position } = require('./models/position');

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.json());

app.get('/positions', (req, res) => {
	Position.find().then((coordinates) => {
		res.send({coordinates});
	}, (e) => {
		res.status(400).send(e);
	});
});

app.get('/positions/:id', (req, res) => {
	var id = req.params.id;
	Position.find({_id: id}).then((coordinates) => {
		res.send(coordinates);
	}, (e) => {
		res.status(400).send(e);
	});
});

app.post('/positions', (req, res) => {
	var newPosition = new Position({
		_id : req.body._id,
	    cordX : req.body.cordX,
	    cordY : req.body.cordY,
	    text : req.body.text
	});

	newPosition.save().then((pos) => {
		res.send(pos);
	}, (e) => {
		res.status(400).send(e);
	});

}); 

app.delete('/positions/:id', (req, res) => {
	var id = req.params.id;
	Position.findByIdAndRemove(id).then((coordinates) => {
		if(!coordinates) {
			return res.status(404).send();
		}
		res.send(coordinates);
	}).catch((e) => {
		res.status(400).send(e);
	});
});

app.patch('/positions/:id', (req, res) => {
	var id = req.params.id;
	var body = _.pick(req.body, ['text']);
	Position.findByIdAndUpdate(id, { $set: body }, { new: true }).then((coordinates) => {
		if(!coordinates) {
			return res.status(404).send();
		}
		res.send({ coordinates });

	}, (e) => {
		res.status(400).send();
	});
});


app.listen(port, function () {
	console.log(`Server is running on ${port}`);
});	